package com.velykyi.model;

import com.velykyi.ResourceException;

import java.io.*;

/**
 * This is class witch work with our model.
 *
 * @author Volodymyr Velykyi
 */
public class StudentModel implements Model {

    /**
     * Get name of student from file.
     *
     * @param filename is a name of file.
     * @return name of student.
     * @throws ResourceException when file name is empty.
     */
    public Student getStudent(String filename) throws ResourceException {
        if (filename.equals("")) {
            throw new ResourceException("You enter nothing.");
        }
        try (StudentFile studentFile = new StudentFile(filename);) {
            BufferedReader buf = new BufferedReader(new InputStreamReader(studentFile.file));
            String name = buf.readLine();
            if (name == null) {
                throw new IOException();
            } else {
                return new Student(name);
            }
        } catch (FileNotFoundException e) {
            System.out.println("File does not exist!");
        } catch (IOException e) {
            System.out.println("File is empty!");
        }
        return null;
    }
}
