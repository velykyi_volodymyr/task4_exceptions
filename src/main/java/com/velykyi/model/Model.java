package com.velykyi.model;

import com.velykyi.ResourceException;

import java.io.IOException;

/**
 * This interface describes model.
 *
 * @author Volodymyr Velykyi
 */
public interface Model {
    /**
     * Get name of student from file.
     *
     * @param filename is a name of file.
     * @return name of student.
     * @throws ResourceException when file name is empty.
     */
    Student getStudent(String filename) throws ResourceException;
}
