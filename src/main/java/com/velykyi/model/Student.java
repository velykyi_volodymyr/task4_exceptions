package com.velykyi.model;

/**
 * This is model of student.
 *
 * @author Volodymyr Velykyi
 */
public class Student {
    /**
     * This is name of student.
     */
    String name;

    /**
     * Initialize object of class.
     *
     * @param name is name of student.
     */
    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
