package com.velykyi.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class keep an object with file witch contain name of student.
 *
 * @author Volodymyr Velykyi
 */
public class StudentFile implements AutoCloseable {
    /**
     * This is our file.
     */
    InputStream file;

    /**
     * Initialize file.
     *
     * @param filename is name of file.
     * @throws FileNotFoundException when file does not exist.
     */
    StudentFile(String filename) throws FileNotFoundException {
        this.file = new FileInputStream(filename);
    }

    /**
     * Close our file.
     *
     * @throws IOException when file is already closed.
     */
    @Override
    public void close() throws IOException {
        this.file.close();
        System.out.println("Closed!");
    }
}
