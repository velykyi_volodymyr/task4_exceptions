package com.velykyi;

import com.velykyi.controller.Controller;
import com.velykyi.model.*;
import com.velykyi.view.*;

/**
 * This is star of app.
 *
 * @author Volodymyr Velykyi
 */
public class Application {

    public static void main(String[] args) {
        View view = new ConsoleView();
        Model model = new StudentModel();
        Controller control = new Controller(view, model);
        control.execute();
    }
}
