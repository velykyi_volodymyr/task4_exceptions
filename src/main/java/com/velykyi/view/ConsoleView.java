package com.velykyi.view;

import com.velykyi.model.Student;

import java.util.Scanner;

/**
 * This class is ui and get data from a user.
 *
 * @author Volodymyr Velykyi
 */
public class ConsoleView implements View {
    /**
     * Get name of file.
     *
     * @return name of file.
     */
    public String getFileName() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter filename: ");
        return scanner.nextLine();
    }

    /**
     * Print name of student to screen.
     *
     * @param student object of student.
     */
    public void showStudent(Student student) {
        if (student != null) {
            System.out.println("Student: " + student.getName());
        }
    }
}
