package com.velykyi.view;

import com.velykyi.model.Student;

/**
 * This interface describe view.
 *
 * @author Volodymyr Velykyi
 */
public interface View {
    /**
     * Get name of file.
     *
     * @return name of file.
     */
    String getFileName();

    /**
     * Print name of student to screen.
     *
     * @param student object of student.
     */
    void showStudent(Student student);
}
