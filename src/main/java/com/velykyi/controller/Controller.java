package com.velykyi.controller;

import com.velykyi.ResourceException;
import com.velykyi.model.*;
import com.velykyi.view.*;

/**
 * This class control View and Model.
 *
 * @author Volodymyr Velykyi
 */
public class Controller {
    /**
     * Our view.
     */
    View view;
    /**
     * Our model.
     */
    Model model;

    /**
     * Initialize controller.
     *
     * @param view  is ui witch get data from user.
     * @param model is our model.
     */
    public Controller(View view, Model model) {
        this.view = view;
        this.model = model;
    }

    /**
     * Create student object, get from user a name of file and get from that file
     * a name of student. Print that name to screen.
     */
    public void execute() {
        Student student = null;
        try {
            student = model.getStudent(view.getFileName());
        } catch (ResourceException e) {
            e.printStackTrace();
        }
        view.showStudent(student);
    }
}
